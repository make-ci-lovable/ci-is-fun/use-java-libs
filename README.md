# use-java-libs

- Update the `pom.xml` file (following instructions of the remote package registry)
- Add a `.gitlab-ci.yml` file (if needed)
  ```yaml
  image: maven:latest

  stages:
    - build

  build:run:amazing:
    stage: build
    script:
      - mvn compile assembly:single
      - java -jar target/main-project.1.0-SNAPSHOT.jar
  ```
